package io.nuvalence.geometry

import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals

class PointTest : BehaviorSpec({
    given("A point locates at (1, 1)") {
        val point = Point(1, 1)

        `when`("checking order against another point to the right") {
            then("the smaller point should return point, the bigger point should be toBeChecked") {
                val toBeChecked = Point(2, 2)
                assertEquals(point.min(toBeChecked), point)
                assertEquals(point.max(toBeChecked), toBeChecked)
            }
        }

        `when`("checking order against another point to the left") {
            then("the smaller point should return toBeChecked, the bigger point should be point") {
                val toBeChecked = Point(0, 2)
                assertEquals(point.min(toBeChecked), toBeChecked)
                assertEquals(point.max(toBeChecked), point)
            }
        }

        `when`("checking order against another point with the same x and smaller y") {
            then("the smaller point should return toBeChecked, the bigger point should be point") {
                val toBeChecked = Point(1, 0)
                assertEquals(point.min(toBeChecked), toBeChecked)
                assertEquals(point.max(toBeChecked), point)
            }
        }

        `when`("checking displace with another point sharing the same y value") {
            then("the distance should be the difference between the two x from both points") {
                val toBeChecked = Point(10, 1)
                assertEquals(point.distanceTo(toBeChecked), 9)
            }
        }

        `when`("checking displace with another point sharing the same x value") {
            then("the distance should be the difference between the two y from both points") {
                val toBeChecked = Point(1, 11)
                assertEquals(point.distanceTo(toBeChecked), 10)
            }
        }

        `when`("checking displace with another point sharing the same x and y values") {
            then("the distance should be the difference should be 0") {
                val toBeChecked = Point(1, 1)
                assertEquals(point.distanceTo(toBeChecked), 0)
            }
        }
    }
})
