package io.nuvalence.geometry

import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class LineTest : BehaviorSpec({

    given("A line starts at (10, 3) and ends at (12, 3)") {
        val line = Line(start = Point(10, 3), end = Point(12, 3))

        `when`("checking intersections against another intersecting line") {
            then("the result should be true and returns the correct intersecting point") {
                val toBeChecked = Line(start = Point(11, 4), end = Point(11, 2))
                line.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(Point(11, 3), result)
                }
            }
        }

        `when`("checking intersections against another intersecting line below it") {
            then("the result should be true and returns the correct intersecting point") {
                val toBeChecked = Line(start = Point(11, 3), end = Point(11, 2))
                line.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(Point(11, 3), result)
                }
            }
        }

        `when`("checking intersections against another intersecting line above it") {
            then("the result should be true and returns the correct intersecting point") {
                val toBeChecked = Line(start = Point(11, 4), end = Point(11, 3))
                line.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(Point(11, 3), result)
                }
            }
        }

        `when`("checking intersections against another intersecting line to the left of it") {
            then("the result should be true and returns the correct intersecting point") {
                val toBeChecked = Line(start = Point(10, 4), end = Point(10, 2))
                line.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(Point(10, 3), result)
                }
            }
        }

        `when`("checking intersections against another intersecting line to the right of it") {
            then("the result should be true and returns the correct intersecting point") {
                val toBeChecked = Line(start = Point(12, 4), end = Point(12, 2))
                line.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(Point(12, 3), result)
                }
            }
        }

        `when`("checking intersections against another intersecting line sharing one end") {
            then("the result should be true and returns the correct intersecting point") {
                val toBeChecked = Line(start = Point(10, 4), end = Point(10, 3))
                line.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(Point(10, 3), result)
                }
            }
        }
    }

    given("A line starts at (2, 3) and ends at (7, 3)") {
        val line = Line(start = Point(2, 3), end = Point(7, 3))

        `when`("checking line containment against another enclosing line along x") {
            then("the result should be true") {
                val toBeChecked = Line(start = Point(3, 3), end = Point(6, 3))
                assertTrue(line.contains(toBeChecked))
            }
        }

        `when`("checking line containment against one non-enclosing line along x") {
            then("the result should be false") {
                val toBeChecked = Line(start = Point(1, 3), end = Point(6, 3))
                assertFalse(line.contains(toBeChecked))
            }
        }
    }

    given("A line starts at (2, 6) and ends at (2, 1)") {
        val line = Line(start = Point(2, 6), end = Point(2, 1))

        `when`("checking line containment against one enclosing line along y") {
            then("the result should be true") {
                val toBeChecked = Line(start = Point(2, 5), end = Point(2, 2))
                assertTrue(line.contains(toBeChecked))
            }
        }

        `when`("checking line containment against one non-enclosing line along y") {
            then("the result should be false") {
                val toBeChecked = Line(start = Point(2, 7), end = Point(2, 2))
                assertFalse(line.contains(toBeChecked))
            }
        }
    }

    given("A vertical line starts at (2, 6) and ends at (2, 1)") {
        val line = Line(start = Point(2, 6), end = Point(2, 1))

        `when`("checking line relationship with one enclosing line") {
            then("the result should be subline") {
                val toBeChecked = Line(start = Point(2, 5), end = Point(2, 2))
                assertEquals(LineRelation.SubLine, line.relationTo(toBeChecked))
            }
        }

        `when`("checking line relationship with overlapping enclosing line") {
            then("the result should be partial") {
                val toBeChecked = Line(start = Point(2, 7), end = Point(2, 2))
                assertEquals(LineRelation.Partial, line.relationTo(toBeChecked))
            }
        }

        `when`("checking line relationship with a line with same start and end") {
            then("the result should be proper") {
                val toBeChecked = Line(start = Point(2, 6), end = Point(2, 1))
                assertEquals(LineRelation.Proper, line.relationTo(toBeChecked))
            }
        }
    }

    given("A horizontal line starts at (2, 4) and ends at (9, 4)") {
        val line = Line(start = Point(2, 4), end = Point(9, 4))

        `when`("checking line relationship with one enclosing line") {
            then("the result should be subline") {
                val toBeChecked = Line(start = Point(3, 4), end = Point(6, 4))
                assertEquals(LineRelation.SubLine, line.relationTo(toBeChecked))
            }
        }

        `when`("checking line relationship with overlapping enclosing line") {
            then("the result should be partial") {
                val toBeChecked = Line(start = Point(4, 4), end = Point(10, 4))
                assertEquals(LineRelation.Partial, line.relationTo(toBeChecked))
            }
        }

        `when`("checking line relationship with a line with same start and end") {
            then("the result should be proper") {
                val toBeChecked = Line(start = Point(2, 4), end = Point(9, 4))
                assertEquals(LineRelation.Proper, line.relationTo(toBeChecked))
            }
        }
    }

    given("A horizontal line starts at (3, 5) and ends at (3, 1)") {
        val line = Line(start = Point(3, 5), end = Point(3, 1))

        `when`("checking line relationship with one intersecting line") {
            then("the result should be Intersect") {
                val toBeChecked = Line(start = Point(2, 4), end = Point(4, 4))
                assertEquals(LineRelation.Intersect, line.relationTo(toBeChecked))
            }
        }
    }
})
