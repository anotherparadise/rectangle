package io.nuvalence.geometry

import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

class RectangleTest : BehaviorSpec({

    /**
     * Tests for Rectangle containing a point
     */
    given("A rectangle locates at (1, 10) and (10, 0)") {
        val rectangle = Rectangle(topLeft = Point(1, 10), bottomRight = Point(10, 0))

        `when`("checking containment of the point at (5, 10)") {
            val point = Point(5, 10)
            then("the result should true") {
                assertTrue(rectangle.contain(point))
            }
        }

        `when`("checking containment of the point at (1, 5)") {
            val point = Point(1, 5)
            then("the result should be true") {
                assertTrue(rectangle.contain(point))
            }
        }

        `when`("checking containment of the point at (10, 5)") {
            val point = Point(10, 5)
            then("the result should be true") {
                assertTrue(rectangle.contain(point))
            }
        }

        `when`("checking containment of the point at (5, 0)") {
            val point = Point(5, 0)
            then("the result should be true") {
                assertTrue(rectangle.contain(point))
            }
        }

        `when`("checking containment of the point at (50, 50)") {
            val point = Point(50, 50)
            then("the result should be false") {
                assertFalse(rectangle.contain(point))
            }
        }
    }

    /**
     * Tests for Rectangle containing another rectangle
     */
    given("A rectangle locates at (4, 8) and (10, 2)") {
        val rectangle = Rectangle(topLeft = Point(4, 8), bottomRight = Point(10, 2))

        `when`("checking containment of a rectangle sharing no borders") {
            val toBeChecked = Rectangle(topLeft = Point(5, 7), bottomRight = Point(7, 5))
            then("the result should be true") {
                assertTrue(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking containment of a rectangle sharing top border") {
            val toBeChecked = Rectangle(topLeft = Point(6, 8), bottomRight = Point(8, 6))
            then("the result should be true") {
                assertTrue(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking containment of a rectangle sharing bottom border") {
            val toBeChecked = Rectangle(topLeft = Point(6, 4), bottomRight = Point(8, 2))
            then("the result should be true") {
                assertTrue(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking containment of a rectangle sharing right border") {
            val toBeChecked = Rectangle(topLeft = Point(8, 6), bottomRight = Point(10, 4))
            then("the result should be true") {
                assertTrue(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking containment of a rectangle sharing left border") {
            val toBeChecked = Rectangle(topLeft = Point(4, 6), bottomRight = Point(6, 4))
            then("the result should be true") {
                assertTrue(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking containment of an identical rectangle") {
            then("the result should be true") {
                assertTrue(rectangle.contain(rectangle))
                assertFalse(rectangle.intersect(rectangle).status)
                assertFalse(rectangle.isAdjacentTo(rectangle).status)
            }
        }

        `when`("checking containment of non overlapping rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(14, 8), bottomRight = Point(20, 2))
            then("the result should be false") {
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(rectangle).status)
                assertFalse(rectangle.isAdjacentTo(rectangle).status)
            }
        }

        `when`("checking containment of sharing border rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(10, 8), bottomRight = Point(14, 4))
            then("the result should be false") {
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(rectangle).status)
                assertFalse(rectangle.isAdjacentTo(rectangle).status)
            }
        }

        `when`("checking containment of an overlapping rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(8, 10), bottomRight = Point(12, 6))
            then("the result should be false") {
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(rectangle).status)
                assertFalse(rectangle.isAdjacentTo(rectangle).status)
            }
        }

        `when`("checking containment of an enclosing rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(2, 10), bottomRight = Point(12, 0))
            then("the result should be false") {
                assertTrue(toBeChecked.contain(rectangle))
                assertFalse(rectangle.contain(toBeChecked))
            }
        }
    }

    /**
     * Tests for Rectangle intersecting with another rectangle
     */
    given("A rectangle locates at (3, 8) and (8, 3)") {
        val rectangle = Rectangle(topLeft = Point(3, 8), bottomRight = Point(8, 3))

        `when`("checking intersection of a rectangle intersecting at the bottom") {
            val toBeChecked = Rectangle(topLeft = Point(4, 4), bottomRight = Point(7, 2))
            then("the result should be true") {
                rectangle.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        Rectangle(topLeft = Point(4, 4), bottomRight = Point(7, 3)),
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking intersection of a rectangle intersecting at the top") {
            val toBeChecked = Rectangle(topLeft = Point(4, 9), bottomRight = Point(7, 7))
            then("the result should be true") {
                rectangle.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        Rectangle(topLeft = Point(4, 8), bottomRight = Point(7, 7)),
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking intersection of a rectangle intersecting on the right") {
            val toBeChecked = Rectangle(topLeft = Point(7, 7), bottomRight = Point(9, 4))
            then("the result should be true") {
                rectangle.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        Rectangle(topLeft = Point(7, 7), bottomRight = Point(8, 4)),
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking intersection of a rectangle intersecting on the left") {
            val toBeChecked = Rectangle(topLeft = Point(2, 7), bottomRight = Point(4, 4))
            then("the result should be true") {
                rectangle.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        Rectangle(topLeft = Point(3, 7), bottomRight = Point(4, 4)),
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking intersection of a rectangle intersecting horizontally") {
            val toBeChecked = Rectangle(topLeft = Point(2, 7), bottomRight = Point(9, 4))
            then("the result should be true") {
                rectangle.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        Rectangle(topLeft = Point(3, 7), bottomRight = Point(8, 4)),
                        result
                    )
                }
            }
        }

        `when`("checking intersection of a rectangle intersecting vertically") {
            val toBeChecked = Rectangle(topLeft = Point(4, 9), bottomRight = Point(7, 2))
            then("the result should be true") {
                rectangle.intersect(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        Rectangle(topLeft = Point(4, 8), bottomRight = Point(7, 3)),
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking intersection of a rectangle which is inside the rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(4, 7), bottomRight = Point(6, 5))
            then("the result should be false") {
                rectangle.intersect(toBeChecked).apply {
                    assertFalse(status)
                    assertNull(result)
                }
                assertTrue(rectangle.contain(toBeChecked))
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }

        `when`("checking intersection of a rectangle which is outside of the rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(4, 10), bottomRight = Point(6, 9))
            then("the result should be false") {
                rectangle.intersect(toBeChecked).apply {
                    assertFalse(status)
                    assertNull(result)
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.isAdjacentTo(toBeChecked).status)
            }
        }
    }

    /**
     * Tests for Rectangle is adjacent to another rectangle horizontally
     */
    given("A rectangle locates at (6, 9) and (11, 4)") {
        val rectangle = Rectangle(topLeft = Point(6, 9), bottomRight = Point(11, 4))

        `when`("checking Subline adjacency with another rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(11, 7), bottomRight = Point(13, 6))
            then("the result should be Subline") {
                rectangle.isAdjacentTo(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        RectangleAdjacencyState.Subline,
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
            }
        }

        `when`("checking Proper adjacency with another rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(11, 9), bottomRight = Point(14, 4))
            then("the result should be Proper") {
                rectangle.isAdjacentTo(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        RectangleAdjacencyState.Proper,
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
            }
        }

        `when`("checking Partial adjacency with another rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(11, 10), bottomRight = Point(14, 5))
            then("the result should be Proper") {
                rectangle.isAdjacentTo(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        RectangleAdjacencyState.Partial,
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
            }
        }

        `when`("checking Apart status with another rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(12, 4), bottomRight = Point(14, 2))
            then("the result should be null and not adjacent") {
                rectangle.isAdjacentTo(toBeChecked).apply {
                    assertFalse(status)
                    assertNull(result)
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
            }
        }
    }

    /**
     * Tests for Rectangle is adjacent to another rectangle vertically
     */
    given("A rectangle locates at (3, 5) and (6, 2)") {
        val rectangle = Rectangle(topLeft = Point(3, 5), bottomRight = Point(6, 2))

        `when`("checking Subline adjacency with another rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(4, 6), bottomRight = Point(5, 5))
            then("the result should be Subline") {
                rectangle.isAdjacentTo(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        RectangleAdjacencyState.Subline,
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
            }
        }

        `when`("checking Proper adjacency with another rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(3, 6), bottomRight = Point(6, 5))
            then("the result should be Proper") {
                rectangle.isAdjacentTo(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        RectangleAdjacencyState.Proper,
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
            }
        }

        `when`("checking Partial adjacency with another rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(2, 6), bottomRight = Point(5, 5))
            then("the result should be Proper") {
                rectangle.isAdjacentTo(toBeChecked).apply {
                    assertTrue(status)
                    assertEquals(
                        RectangleAdjacencyState.Partial,
                        result
                    )
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
            }
        }

        `when`("checking Apart status with another rectangle") {
            val toBeChecked = Rectangle(topLeft = Point(1, 7), bottomRight = Point(2, 6))
            then("the result should be null and not adjacent") {
                rectangle.isAdjacentTo(toBeChecked).apply {
                    assertFalse(status)
                    assertNull(result)
                }
                assertFalse(rectangle.contain(toBeChecked))
                assertFalse(rectangle.intersect(toBeChecked).status)
            }
        }
    }
})
