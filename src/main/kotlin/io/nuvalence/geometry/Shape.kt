package io.nuvalence.geometry

/**
 * [Shape] represents a geometric shape.
 *
 */
interface Shape<T, I, A> {

    /**
     * [intersect] - this function checks if the current shape intersect with [shape].
     *
     * @param shape - target shape of the type [T].
     * @return [OperationResult] - It contains the status of the check along with possible additional information about the check.
     */
    fun intersect(shape: T): OperationResult<I>

    /**
     * [contain] - this function checks if the current shape contains [shape].
     *
     * @param shape - target shape of the type [T].
     * @return [Boolean] - true when check is successful, false otherwise
     */
    fun contain(shape: T): Boolean

    /**
     * [isAdjacentTo] - this function checks if the current shape is adjacent to [shape].
     *
     * @param shape - target shape of the type [T].
     * @return [OperationResult] - It contains the status of the check along with possible additional information about the check.
     */
    fun isAdjacentTo(shape: T): OperationResult<A>
}

/**
 * Operation result - This is a data holder which contains the operation status along with additional possible data.
 *
 * @param R - generic type, which should be defined at the user-site
 * @property status - true for successful check, false otherwise
 * @property result - additional information about the operation, it can be null
 * @constructor Create empty Operation result
 */
class OperationResult<R>(val status: Boolean, val result: R?)
