package io.nuvalence.geometry

import io.nuvalence.geometry.LineRelation.SubLine
import io.nuvalence.geometry.RectangleAdjacencyState.NotDefined
import io.nuvalence.geometry.RectangleAdjacencyState.Partial
import io.nuvalence.geometry.RectangleAdjacencyState.Proper

/**
 * Rectangle represents an area in a coordinate system that is confined by Rectangle's top left and bottom right points
 *
 * @constructor
 *
 * @param topLeft - a [Point] represents the top left corner of a [Rectangle]
 * @param bottomRight - a [Point] represents the bottom right corner of a [Rectangle]
 */
data class Rectangle private constructor(val topLeft: Point, val bottomRight: Point) :
    Shape<Rectangle, Rectangle, RectangleAdjacencyState> {
    private val topRight by lazy {
        Point(bottomRight.x, topLeft.y)
    }

    private val bottomLeft by lazy {
        Point(topLeft.x, bottomRight.y)
    }

    /**
     * Top side - the upper side of the rectangle
     */
    private val topSide by lazy {
        Line(start = topLeft, end = topRight)
    }

    /**
     * Right side - the right side of the rectangle
     */
    private val rightSide by lazy {
        Line(start = topRight, end = bottomRight)
    }

    /**
     * Bottom side - the bottom side of the rectangle
     */
    private val bottomSide by lazy {
        Line(start = bottomLeft, end = bottomRight)
    }

    /**
     * Left side - the left side of the rectangle
     */
    private val leftSide by lazy {
        Line(start = topLeft, end = bottomLeft)
    }

    companion object {
        operator fun invoke(topLeft: Point, bottomRight: Point): Rectangle {
            require(topLeft.x < bottomRight.x && topLeft.y > bottomRight.y) {
                "topLeft needs to be on top and left side of bottomRight"
            }
            return Rectangle(topLeft, bottomRight)
        }
    }

    /**
     * Intersects - check if this [Rectangle] intersects with the [shape]
     *
     * @param shape - [Rectangle] needs to bbe checked
     * @return a [OperationResult] object, when intersection check is successful, the status will be true and result will be another rectangle.
     *  Otherwise, the status will be false and result will be null.
     */
    override fun intersect(shape: Rectangle): OperationResult<Rectangle> {
        val intersectHorizontally = topLeft.x.coerceAtLeast(shape.topLeft.x) < topRight.x.coerceAtMost(shape.topRight.x)
        val intersectVertically =
            topLeft.y.coerceAtMost(shape.topLeft.y) > bottomRight.y.coerceAtLeast(shape.bottomRight.y)

        if (intersectHorizontally && intersectVertically && !contain(shape)) {
            val x = topLeft.x.coerceAtLeast(shape.topLeft.x)
            val y = topLeft.y.coerceAtMost(shape.topLeft.y)
            val start = Point(x, y)
            val end = Point(
                x + (topRight.x.coerceAtMost(shape.topRight.x) - topLeft.x.coerceAtLeast(shape.topLeft.x)),
                y - (topLeft.y.coerceAtMost(shape.topLeft.y) - bottomRight.y.coerceAtLeast(shape.bottomRight.y))
            )
            return OperationResult(true, Rectangle(topLeft = start, bottomRight = end))
        }
        return OperationResult(false, null)
    }

    /**
     * Check if this [Rectangle] fully contains this [shape]
     *
     * @param shape - rectangle to be contained
     * @return true if the [shape] is fully contained by this [Rectangle]
     */
    override fun contain(shape: Rectangle): Boolean {
        return contain(shape.topLeft) && contain(shape.bottomRight)
    }

    /**
     * Is adjacent to - Check if two rectangles are adjacent to each other, when adjacency happens, the possible states are:
     * Partial - when one side of one rectangle overlap withe one side of the other rectangle
     * Proper - when overlapping sides from both rectangle are the same length
     * Subline - when one side of one rectangle fully contains one side of the other rectangle
     *
     * @param shape
     * @return a [OperationResult] object, when adjacency check is successful, the status will be true and result will be a RectangleAdjacencyState indicting the type of adjacency.
     *  Otherwise, the status will be false and result will be null.
     */
    override fun isAdjacentTo(shape: Rectangle): OperationResult<RectangleAdjacencyState> {
        val validLineRelations = listOf(LineRelation.SubLine, LineRelation.Proper, LineRelation.Partial)
        val sidesToBeChecked = listOf(
            topSide to shape.bottomSide,
            bottomSide to shape.topSide,
            leftSide to shape.rightSide,
            rightSide to shape.leftSide
        )

        val adjacencyStates =
            sidesToBeChecked.map { it.first.relationTo(it.second) }.filter { it in validLineRelations }
        return if (adjacencyStates.isEmpty()) {
            OperationResult(false, null)
        } else {
            OperationResult(true, RectangleAdjacencyState.fromLineRelation(adjacencyStates.first()))
        }
    }

    /**
     * Contains - check if the [Rectangle] contains the [point]
     *
     * @param point - point to be checked
     * @return true if the [Rectangle] contains the [point] or false otherwise
     */
    internal fun contain(point: Point): Boolean {
        point.apply {
            return x >= topLeft.x && x <= bottomRight.x && y <= topLeft.y && y >= bottomRight.y
        }
    }
}

/**
 * Rectangle adjacency state - the possible values are:
 * [Proper] - when two adjacent sides are identical with the same start and end points.
 * [SubLine] - when one of the adjacent line contains the other.
 * [Partial] - when one of teh adjacent line fully contains the other, but not have the same length
 * [NotDefined] - when none of the above conditions are met.
 * @constructor Create empty Rectangle adjacency state
 */
enum class RectangleAdjacencyState {
    Subline, Proper, Partial, NotDefined;

    companion object {
        fun fromLineRelation(lineRelation: LineRelation) = when (lineRelation) {
            LineRelation.SubLine -> Subline
            LineRelation.Proper -> Proper
            LineRelation.Partial -> Partial
            else -> NotDefined
        }
    }
}
