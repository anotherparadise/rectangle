package io.nuvalence.geometry

import io.nuvalence.geometry.LineOrientation.Horizontal
import io.nuvalence.geometry.LineOrientation.Vertical
import io.nuvalence.geometry.LineRelation.Apart
import io.nuvalence.geometry.LineRelation.Intersect
import io.nuvalence.geometry.LineRelation.Parallel
import io.nuvalence.geometry.LineRelation.Partial
import io.nuvalence.geometry.LineRelation.Proper
import io.nuvalence.geometry.LineRelation.SubLine
import kotlin.math.absoluteValue

/**
 * [Line] represents line segment in a coordinate system. Currently, the line can be either horizontal or vertical. Slope is NOT supported.
 *
 * @property start - start of the [Line]
 * @property end - end of the [Line]
 * @constructor Create a [Line] from [start] to [end]
 */
data class Line private constructor(val start: Point, val end: Point) {

    private val orientation = if (start.x == end.x) LineOrientation.Vertical else LineOrientation.Horizontal

    /**
     * Length - distance from start to end, the value is always a positive integer.
     */
    private val length by lazy {
        (
            when (orientation) {
                Vertical -> {
                    end.y - start.y
                }

                Horizontal -> {
                    end.x - start.x
                }
            }
            ).absoluteValue
    }

    companion object {
        operator fun invoke(start: Point, end: Point): Line {
            require(start != end) {
                "Can't use one point to construct a line."
            }
            require(start.x == end.x || start.y == end.y) {
                "Either a horizontal or vertical line is supported."
            }
            require(start.x <= end.x && start.y >= end.y) {
                "start needs to be before end either horizontally or vertically"
            }
            return Line(start, end)
        }
    }

    /**
     * Intersects - check if two lines intersect with each other, if intersection happens, we will return true along with the point where they intersect.
     *
     * @param anotherLine - line to check the intersection.
     * @return a [Pair] with first indicating the status of the intersection, second indicating the intersecting point.
     */
    fun intersect(anotherLine: Line): OperationResult<Point> {
        return if (orientation == anotherLine.orientation) {
            OperationResult(false, null)
        } else {
            val horizontalLine = if (orientation == LineOrientation.Horizontal) this else anotherLine
            val verticalLine = if (orientation == LineOrientation.Vertical) this else anotherLine
            val verticalLineBeforeOrAfterHorizontalLine =
                verticalLine.start.x < horizontalLine.start.x || verticalLine.start.x > horizontalLine.end.x
            val horizontalLineAboveOrBelowVerticalLine =
                horizontalLine.start.y > verticalLine.start.y || horizontalLine.start.y < verticalLine.end.y
            if (verticalLineBeforeOrAfterHorizontalLine || horizontalLineAboveOrBelowVerticalLine) {
                OperationResult(false, null)
            } else {
                OperationResult(true, Point(verticalLine.start.x, horizontalLine.start.y))
            }
        }
    }

    /**
     * Relation to - this method check the relationship between two lines. Possible values are :
     *      Apart - lines don't touch each other and are not parallel to each other.
     *      Proper - When two lines have the same start and end.
     *      Subline - When one line contains the other line fully and both line don't have the same length
     *      Intersect - When one line is perpendicular to another.
     *      Partial - When one line partially contains another line.
     * @param anotherLine
     * @return
     */
    fun relationTo(anotherLine: Line): LineRelation {
        if (orientation == anotherLine.orientation) {
            if ((start.x == anotherLine.start.x && orientation == Vertical) || (start.y == anotherLine.start.y && orientation == Horizontal)) {
                val firstPoint =
                    if (orientation == Vertical) start.max(anotherLine.start) else start.min(anotherLine.start)
                val secondPoint = if (orientation == Vertical) end.min(anotherLine.end) else end.max(anotherLine.end)

                if (firstPoint.distanceTo(secondPoint) >= length + anotherLine.length) {
                    return Apart
                } else {
                    if (this == anotherLine) {
                        return Proper
                    }
                    return if (this.contains(anotherLine) || anotherLine.contains(this)) {
                        SubLine
                    } else {
                        Partial
                    }
                }
            } else {
                return Parallel
            }
        } else if (intersect(anotherLine).status) {
            return Intersect
        } else {
            return Apart
        }
    }

    /**
     * Contains - One line fully contains the other line. When two lines with the same start and end, the function will return false as they are considered as Proper.
     *
     * @param anotherLine
     * @return
     */
    internal fun contains(anotherLine: Line): Boolean {
        return when (orientation) {
            Vertical -> {
                start.y > anotherLine.start.y && end.y < anotherLine.end.y
            }

            Horizontal -> {
                start.x < anotherLine.start.x && end.x > anotherLine.end.x
            }
        }
    }
}

/**
 * Line orientation - It can be either [Vertical] or [Horizontal].
 *
 * @constructor Create empty Line orientation.
 */
enum class LineOrientation {
    Vertical,
    Horizontal
}

/**
 * Line relation represents positional relationship between two line. The possible values are proper, subline, partial and apart.
 * [Proper] - when two lines are identical with the same start and end points.
 * [SubLine] - when one line contains the other.
 * [Intersect] - when two lines are perpendicular to each other.
 * [Partial] - when connect two lines, the new line's length is bigger than either of the two.
 * [Apart] - when both lines do not share any points.
 * [Parallel] - when both lines are parallel to each other.
 * @constructor Create empty Line relation.
 */
enum class LineRelation {
    SubLine, Proper, Partial, Intersect, Apart, Parallel,
}
