package io.nuvalence.geometry

import kotlin.math.absoluteValue

/**
 * [Point] represents a position in a coordinate system.
 *
 * @property x - x coordinate
 * @property y - y coordinate
 * @constructor Create a [Point] with position at {[x], [y]}
 */
data class Point(val x: Int, val y: Int) {
    operator fun compareTo(anotherPoint: Point) =
        if (x == anotherPoint.x) y - anotherPoint.y else x - anotherPoint.x

    /**
     * Min - this function returns the point with smaller x value when both points have the same y values, or the point with smaller y value when both have the same x values.
     *
     * @param anotherPoint
     */
    fun min(anotherPoint: Point) = if (this < anotherPoint) this else anotherPoint

    /**
     * Max - this function returns the point with bigger x value when both points have the same y values, or the point with bigger y value when both have the same x values.
     *
     * @param anotherPoint
     */
    fun max(anotherPoint: Point) = if (this > anotherPoint) this else anotherPoint

    /**
     * Length - the length from this point to [anotherPoint], currently implementation only support points that forms a line which is perpendicular to either x-axis or y-axis
     *
     * @param anotherPoint
     */
    fun distanceTo(anotherPoint: Point) =
        if (x == anotherPoint.x) (y - anotherPoint.y).absoluteValue else (x - anotherPoint.x).absoluteValue
}
