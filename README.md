# Project structure and setup

This project is implemented in Kotlin and its feature set includes retangle containment, intersection and adjacency detections.

## Tools used in the project

*   `JDK` **18**
*   `Gradle` **7.5.1**

## Depenencies
*   `kotest` - a testing framework that supports multiple testing styles. In this project, I used behavior driven style for every testing scenario.
*   `jacoco` -  a code coverage reports generator for Kotin/Java projects.
*   `jib` - plugin to help build docker image for Kotlin/Java projects.
*   `dokka` - Api documentation generator.
*   `ktlint` - a linter to auto-format Kotlin code.

## Feature overview

*   [x] Rectangle has features like containment, intersection and adjacency detections
*   [x] Fully component based design, every component is a custom data structure.
*   [x] Features have extensive testing with 93% code coverage
*   [x] Multiple API documentation formats support including KDoc, Javadoc, HTML and markdown.
*   [x] Easily build with docker

## Contents

*   [Getting started](#getting-started)
    *   [Run the application](#run-the-application)
    *   [Reports](#reports)
    *   [Linux runnable](#linux-certified)
*   [Notes](#notes)

## Getting Started

### Requirements

* `JDK` **18** - Java versioning can be tricky, I use [asdf](https://asdf-vm.com/) for JDK management on my local machine.


### Run the application

1) Use git to clone this repository into your computer.

```
git clone git@gitlab.com:anotherparadise/rectangle.git
```

2) Go to the project folder

```bash
cd rectangle
```

3) Run test

```bash
./gradlew clean test
```
There are 54 tests, 17 for `Line` related tests, 6 for `Point` related tests and 31 for `Rectangle` related test.

### Reports

1) Generate reports for testing

```bash
./gradlew clean test
```
2) Testing report can be found at `rectangle/build/reports/tests/test`, 
you can view the report by opening the `index.html` in the same folder.<img src="screenshots/test.png" alt="Alt text" title="tests report">
3) Code coverage report can be found at `rectangle/build/jacocoHtml`,
   you can view the report by opening the `index.html` in the same folder.
<img src="screenshots/jacoco.png" alt="Alt text" title="code coverage">
4) Generate api doc

```bash
./gradlew clean dokkaHtml
```
<img src="screenshots/api.png" alt="Alt text" title="tests report">
5) Api documentation can be found at `rectangle/build/dokka/html`,
   you can view the report by opening the `index.html` in the same folder.

### Linux certified
In order to prove that the app can execute in a linux environment, I have command to build a jar file and bundle it inside a docker image. When a container is created off the image, a message "Howdy!" will be printed to the console to prove the Java app is running.

Make sure docker engine is running before running the following command.

1) build docker image 

```bash
cd rectangle
./gradlew jibDockerBuild
```

2) make sure image has been successfully built

```bash
docker images | grep rectangle
take-home/rectangle              latest                      3b50a48f83ed   52 years ago    223MB
```

3) Run the app to see a welcome message, here I use `--platform linux/amd64` flag as this is needed when running the command on a Mac with m1 chip. If you run it with Windows, then this is not necessary.

```bash
docker run --platform linux/amd64 --rm take-home/rectangle
Howdy!
```

4) After ssh to the container and print os info with `cat /etc/os-release`, we got below information to prove the app is indeed running inside linux.
```
PRETTY_NAME="Ubuntu 22.04.1 LTS"
NAME="Ubuntu"
VERSION_ID="22.04"
VERSION="22.04.1 LTS (Jammy Jellyfish)"
VERSION_CODENAME=jammy
ID=ubuntu
ID_LIKE=debian
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
UBUNTU_CODENAME=jammy
```

## Notes

There are some limitations about the design, all the basic primitive types(Point, Line and Rectangles) only work when the lines are either parallel to x-axis or y-axis.
As there is no interface provided for the assignment, it's hard to plugin my implementation to an exiting engine to test its effectiveness and accuracy. 
I'm happy to discuss the design and implementation.